#!/usr/bin/env python3

# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import socket
import http.client
import urllib.parse
from activipy import vocab, core
from datetime import datetime


def get_server_conn(host, port):
    sockobj = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sockobj.connect((host, int(port)))
    return sockobj


def getConnectionLocation(objID):
    purl = urllib.parse.urlparse(objID)
    addr = purl.netloc
    if ":" in addr:
        addr, port = addr.split(":")
        port = int(port)
    else:
        port = 8080
    path = purl.path
    return addr, port, path


def getObject(objID):
    addr, port, path = getConnectionLocation(objID)
    conn = http.client.HTTPConnection(addr, port)
    header = {"Accept": 'application/ld+json; '
              'profile="https://www.w3.org/ns/activitystreams"'}
    conn.request("GET", path, None, header)
    resp = conn.getresponse()
    respBody = resp.read()
    conn.close()
    if "application/json" in resp.getheader("Content-Type"):
        respBody = json.loads(respBody)
        obj = core.ASObj(respBody)
        return obj
    else:
        return None


def postToOutbox(outboxID, obj, debug=0):
    body = json.dumps(obj.json())
    addr, port, path = getConnectionLocation(outboxID)
    conn = http.client.HTTPConnection(addr, port)
    conn.request("POST", path, body,
                 {"Content-Type": 'application/ld+json; '
                  'profile="https://www.w3.org/ns/activitystreams"'})
    resp = conn.getresponse()
    respBody = resp.read()
    conn.close()
    if debug > 0:
        print("\n--- POST begin ---\n")
        print(resp.status)
        print(resp.headers)
        print(respBody)
        print("\n--- POST end ---\n")
    location = resp.getheader("Location")
    if location:
        return location, None
    else:
        return None, respBody


def createNote(outboxID, to, cc, bto, bcc, summary, body):
    create = vocab.Create(object=vocab.Note(to=to, cc=cc, bto=bto, bcc=bcc,
                                            summary=summary, content=body))
    return postToOutbox(outboxID, create)


def createActor(actorID, name):
    postPath = actorID + "/outbox"
    activity = vocab.Create(object=vocab.Person(displayName=name))
    return postToOutbox(postPath, activity)
