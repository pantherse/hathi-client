#!/usr/bin/env python3

# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Database functions
"""

import sqlite3

class dbException(Exception):
    def __init__(self,dErrorArguments):
        Exception.__init__(self,"Something went wrong in dbLib! {}".format(dErrorArguments))

class database():
    """
    Create the connection, cursor, and setup the users table
    """
    def __init__(self, filename):
        try:
            self.conn = sqlite3.connect(filename)
            self.cursor = self.conn.cursor()
            self.cursor.execute('CREATE TABLE IF NOT EXISTS users (name, address)')
            self.conn.commit()
            self.goodConnection = True
            self.errors = []
        except PermissionError as permError:
            self.goodConnection = False
            self.errors.append(permError)
            raise
        except sqlite.DatabaseError as dbError:
            self.goodConnection = False
            self.errors.append(dbError)
            raise
        except Exception as e:
            self.goodConnection = False
            self.errors.append(e)
            raise

    """
    Add a user to a group
    """
    def add_user(self, name, address):
        if self.goodConnection:
            try:
                self.cursor.execute('INSERT INTO users  VALUES(?, ?)', (name, address))
                self.conn.commit()
            except sqlite3.DatabaseError as dbError:
                self.errors.append(dbError)
                raise
        else:
            raise self.errors[0]

    """
    Remove a user based on address -- more unique than a name
    """
    def del_user_address(self, address):
        if self.goodConnection:
            try:
                self.cursor.execute("SELECT rowid FROM users WHERE address = ?", (address,))
                data = self.cursor.fetchone()
                if data == None:
                    raise ValueError("No Such address: {}".format(address))
                else:
                    self.cursor.execute('DELETE FROM users WHERE address=?', (address,))
                    self.conn.commit()
            except sqlite3.DatabaseError as dbError:
                self.errors.append(dbError)
                raise
            except sqlite3.Error as e:
                self.errors.append(e)
                raise
        else:
            raise self.errors[0]

    """
    Get all the users in a group
    """
    def get_list(self):
        if self.goodConnection:
            results = []
            for row in self.cursor.execute('SELECT name FROM users'):
                results.append(row)
            return results
        else:
            raise self.errors[0]

    """
    Get all errors for checking
    """
    def get_errors(self):
        return self.errors

    """
    Clean up connection when object is destroyed
    """
    def __del__(self):
        if self.goodConnection:
            self.conn.close()
