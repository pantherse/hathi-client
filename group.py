#!/usr/bin/env python3

# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" group.py
Connect to a server to get messages for a group and resends them to the
members of the group. It also manages the group members through messages
Outline can be found in group-flow-chart.asciiart
"""

import argparse
import configparser
import dbLib as db
import hathiLib
import logging
import os
import socket
import sys

"""
Argument parsing
"""
def create_args():
    parser = argparse.ArgumentParser(description='Hathi Groups')
    parser.add_argument('-C', '--config',
                        default=[os.getenv("HOME") + "/.config/hathi/"],
                        dest='config',
                        help='Specify the config file location',
                        nargs=1,
                        type=str)
    parser.add_argument('-L', '--log',
                        default=[os.getenv('HOME') + "/.hathi/"],
                        dest='log',
                        help='Specify the location of the log file',
                        nargs=1,
                        type=str)
    parser.add_argument('-D', '--db',
                        default=[os.getenv("HOME") + "/.hathi/"],
                        dest='db',
                        help='Specify the location of the database',
                        nargs=1,
                        type=str)
    parser.add_argument('-H', '--host',
                        dest='host',
                        help='Specify a hostname',
                        nargs=1,
                        type=str)
    parser.add_argument('-p','--port',
                        dest='port',
                        help='Specify a port',
                        nargs=1,
                        type=int)
    parser.add_argument('-d','--delay',
                        dest='delay',
                        help='Specify how often to check for new messages',
                        nargs=1,
                        type=int)
    parser.add_argument('file',
                        help='Specify the name of the database file',
                        type=str)

    return parser.parse_args()

"""
Validate user input
"""
def validate_input(args):
    # Check filepath information
    if not os.path.isdir(args.config[0]):
        sys.stderr.write("Could not find directory {}\n".format(args.config[0]))
        sys.exit(1)
    if not os.path.isdir(args.log[0]):
        sys.stderr.write("Could not find directory {}\n".format(args.log[0]))
        sys.exit(1)
    if not os.access(args.log[0], os.W_OK):
        sys.stderr.write("Unable to write to directory {}. Please add write permissions".format(args.log[0]))
        sys.exit(1)
    if not os.path.isdir(args.db[0]):
        sys.stderr.write("Could not find directory {}\n".format(args.db[0]))
        sys.exit(1)
    if not os.access(args.db[0], os.W_OK):
        sys.stderr.write("Unable to write to directory {}. Please add write permissions".format(args.db[0]))
        sys.exit(1)

"""
Add filenames for config and log
"""
def create_filepath(args):
    configFile = args.config[0] + "group.ini"
    logFile = args.log[0] + "hathi-group.log"
    dbFile = args.db[0] + args.file
    return configFile, logFile, dbFile

"""
Read in config file, skipping any values set in arguments
"""
def import_config_file(configFile, args):
    try:
        config = configparser.ConfigParser()
        config.read(configFile)
        if not args.host:
            serverName = config['DEFAULT']['server']
        else:
            serverName = args.host[0]
        if not args.port:
            serverPort = config['DEFAULT']['port']
        else:
            serverPort = args.port[0]
        if not args.delay:
            checkTime = config['DEFAULT']['check_time']
        else:
            checkTime = args.delay[0]
    except KeyError:
        sys.stderr.write("Unable to load configuration at {}\nTry running config script\n".format(configFile))
        sys.exit(1)

"""
Validate host information
"""
def check_host(serverName, serverPort):
    try:
        socket.inet_aton(serverName)
    except socket.error:
        sys.stderr.write("Invalid host address: {}\n".format(serverName))
        exit(1)

    try:
        serverPort = int(serverPort)
    except ValueError:
        sys.stderr.write("Port is not a number: {}\n".format(serverPort))
        exit(1)

"""
Generate an object of the dbLib class
"""
def create_database(dbFile):
    return db.database(dbFile)

def close_database(dbFile):
    del dbFile

"""
Create a socket object
"""
def create_socket(serverName, serverPort):
    try:
        sockobj = hathiLib.get_server_conn(serverName, serverPort)
        return sockobj
    except ConnectionRefusedError:
        sys.stderr.write("Unable to connect to server {}. Is the server running?\n".format(serverName))
        sys.exit(1)

def close_socket(sockobj):
    sockobj.close()

if __name__ == "__main__":
    args = create_args()

#logging.info("getting information")
#message = ['get groups', time]
#for line in message:
#    sockobj.send(line.encode())
#    data = sockobj.recv(1024)
#    print(repr(data))

"""
Close connections and exit
"""
sys.exit()
