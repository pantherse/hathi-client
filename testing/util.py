#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

APCoreFields = ["subject", "relationship", "actor", "attributedTo",
                "attachment", "attachments", "author", "bcc", "bto", "cc",
                "context", "current", "first", "generator", "icon", "image",
                "inReplyTo", "items", "instrument", "orderedItems", "last",
                "location", "next", "object", "oneOf", "anyOf", "origin",
                "accuracy", "prev", "preview", "provider", "replies", "result",
                "scope", "partOf", "tag", "target", "to", "url", "alias",
                "altitude", "content", "contentMap", "displayName",
                "displayNameMap", "downstreamDuplicates", "duration",
                "durationIso", "endTime", "height", "href", "hreflang", "id"
                "latitude", "longitude", "mediaType", "objectType", "priority",
                "published", "radius", "rating", "rel", "startIndex",
                "startTime", "summary", "summaryMap", "title", "titleMap",
                "totalItems", "units", "updated", "upstreamDuplicates", "verb",
                "width", "describes"]

def makeIDFromPath(host, port, path):
    path = path.lstrip("/")
    return "http://%s:%i/%s" % (host, port, path)
