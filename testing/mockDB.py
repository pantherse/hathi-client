#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from activipy import vocab, core

def buildStartingDB(hostname, port):
    db = {}
    addrRoot = "http://%s:%i/" % (hostname, port)
    noteID = addrRoot + "0"
    db[noteID] = vocab.Note(id=noteID, summary="Foo bar", content="Foo bar baz quux!")
    createID = addrRoot + "1"
    db[createID] = vocab.Create(id=createID, object=noteID)
    inboxID = addrRoot + "tester/inbox"
    db[inboxID] = vocab.OrderedCollection(id=inboxID)
    outboxID = addrRoot + "tester/outbox"
    db[outboxID] = vocab.OrderedCollection(id=outboxID)
    followingID = addrRoot + "tester/following"
    db[followingID] = vocab.OrderedCollection(id=followingID)
    followersID = addrRoot + "tester/followers"
    db[followersID] = vocab.OrderedCollection(id=followersID)
    personID = addrRoot + "tester"
    db[personID] = vocab.Person(id=personID, displayName="Foo", inbox=inboxID,
                                outbox=outboxID, following=followingID,
                                followers=followersID)
    return db
