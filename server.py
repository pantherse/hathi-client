#!/usr/bin/env python
# # -*- coding: utf-8 -*-

# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""server.py
created as strawman

set up to listen on localhost port 50007, 50008, 50009
"""

import sys, time
from select import select
import socket
import argparse


parser = argparse.ArgumentParser( description='A simple echo server', epilog="run python3 server.py before running the client in another shell")
parser.add_argument('-host', default='',
                    help='a specific host if not the localhost')
parser.add_argument('-port', type=int, default=50007,
                    help='a specific port (default: 50007)')

args = parser.parse_args()

def now(): return time.ctime(time.time())

myHost = ''
myPort = 50008  #just some crazy big port not reserved
if len(sys.argv) == 3:  # can pass host and port
    myHost, myPort = sys.argv[1:]

sockobj = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # create a socket object
sockobj.bind((myHost, myPort))
sockobj.listen(5)

while True:
    connection, address = sockobj.accept()
    print('server connected by', address)
    while True:
        data = connection.recv(1024) # limit size
        if not data: break
        tosend = 'echo: %s' % data
        print(tosend)
        connection.send(tosend.encode())
    connection.close()



## End of file
