#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

coreFields = ["subject", "relationship", "actor", "attributedTo", "attachment",
              "attachments", "author", "bcc", "bto", "cc", "context",
              "current", "first", "generator", "icon", "image", "inReplyTo",
              "items", "instrument", "orderedItems", "last", "location",
              "next", "object", "oneOf", "anyOf", "origin", "accuracy", "prev",
              "preview", "provider", "replies", "result", "scope", "partOf",
              "tag", "target", "to", "url", "alias", "altitude", "content",
              "contentMap", "displayName", "displayNameMap",
              "downstreamDuplicates", "duration", "durationIso", "endTime",
              "height", "href", "hreflang", "id", "latitude", "longitude",
              "mediaType", "objectType", "priority", "published", "radius",
              "rating", "rel", "startIndex", "startTime", "summary",
              "summaryMap", "title", "titleMap", "totalItems", "units",
              "updated", "upstreamDuplicates", "verb", "width", "describes",
              "@type", "@id", "@context"]

coreTypes = ["Accept", "Activity", "IntrasnitiveActivity", "Actor", "Add",
             "Album", "Announce", "Application", "Arrive", "Article", "Audio",
             "Block", "Collection", "CollectionPage", "Relationship",
             "Content", "Create", "Delete", "Dislike", "Document", "Event",
             "Folder", "Follow", "Flag", "Group", "Ignore", "Image", "Invite",
             "Join", "Leave", "Like", "Link", "Mention", "Note", "Object",
             "Offer", "OrderedCollection", "OrderedCollectionPage",
             "Organization", "Page", "person", "Place", "Process", "Profile",
             "Question", "Reject", "Remove", "Service", "Story",
             "TentativeAccept", "TentativeReject", "Undo", "Update", "Video",
             "Experience", "View", "Listen", "Read", "Move", "Travel"]


def validateContentType(contType):
    first = 'application/ld+json; ' \
            'profile="https://www.w3.org/ns/activitystreams"'
    second = "application/activity+json"
    perfect = False
    usable = False
    if first in contType:
        perfect = True
        usable = True
    elif second in contType:
        usable = True
    return perfect, usable


def buildAliases(obj):
    primary = "https://www.w3.org/ns/activitystreams"
    alt = "http://www.w3.org/ns/activitystreams"  # http instead of https
    specials = {}
    aliases = {}
    context = obj.get("@context")

    def handleDict(d):
        for fname, value in d.items():
            if fname.startswith("@"):
                specials[fname] = value
            else:
                aliases[fname] = value
    if context is None:
        return aliases, specials, True  # Empty context is acceptable
    elif isinstance(context, str):
        specials["@vocab"] = context
    elif isinstance(context, dict):
        handleDict(context)
    elif isinstance(context, (list, tuple)):
        for i in context:
            if isinstance(i, dict):
                handleDict(i)
            elif isinstance(i, str):
                specials["@vocab"] = i
            else:  # unknown
                pass
    else:  # no idea what to do with this
        pass
    contGood = specials.get("@vocab") in (primary, alt, None)
    return aliases, specials, contGood


def isKnownToken(fieldName, aliases, valids):
    if ":" in fieldName:
        nspace, fieldName = fieldName.split(":")
    else:
        nspace = ""
    if nspace in ("", "as"):  # Core ActivityPub
        return fieldName in valids
    elif nspace in aliases:  # An extension, return which extension
        return aliases[nspace]
    else:  # Nope
        return False


def isKnownField(fieldName, aliases):
    return isKnownToken(fieldName, aliases, coreFields)


def isKnownType(typeName, aliases):
    return isKnownToken(typeName, aliases, coreTypes)


def validateMappableField(field, value):
    strForms = ("displayName", "summary", "content")
    dictForms = ("displayNameMap", "summaryMap", "contentMap")
    if field in strForms:
        return isinstance(value, str)
    elif field in dictForms:
        return isinstance(value, dict)
    else:
        return None


def trawlObject(obj, aliases, fieldChain=""):
    reports = []
    for field, value in obj.items():
        # Check fields
        if field == "@context":
            continue  # Handled before this function is called
        elif field in ("type", "id"):
            # Programs *should* use @type/@id, but *may* use type/id
            reports.append("Alias used: %s" % field)
        elif validateMappableField(field, value) is False:
            # Some fields have a string and a map form.
            reports.append("Misused field %s" % field)
        else:
            if isKnownField(field, aliases) is False:
                reports.append("Unknown field: %s" % field)
        # Check object type(s)
        if field in ("@type", "type"):
            if isinstance(value, str):
                if isKnownType(value, aliases) is False:
                    reports.append("Unknown type: %s" % value)
            elif isinstance(value, (tuple, list)):
                for t in value:
                    if isKnownType(t, aliases) is False:
                        reports.append("Unknown type: %s" % t)
        # Recurse on embedded objects
        link = fieldChain + ":" + field
        if isinstance(value, dict):
            if ("type" in value) or ("@type" in value):
                reports += trawlObject(value, aliases, link)
        elif isinstance(value, (list, tuple)):
            for item in value:
                if isinstance(item, dict):
                    if ("type" in value) or ("@type" in value):
                        reports += trawlObject(item, aliases, link)
    return reports


def validateGETObject(obj, headers):
    reports = []
    ctype = headers.get("Content-type")
    # Content-type *must* contain one of two values
    perfect, usable = validateContentType(ctype)
    if (perfect is False) and (usable is True):
        reports.append("Content-type using alternate type")
    elif (perfect is False) and (usable is False):
        reports.append("Incorrect Content-type")
        return False, reports, {}, {}
    if ("@type" not in obj) and ("type" not in obj):
        reports.append("Object has no @type")
        return False, reports, {}, {}
    # Build the database of alises this packet uses
    # Also validate the core vocabularly it is using
    aliases, specials, contGood = buildAliases(obj)
    if contGood is False:
        reports.append("Invalid @context vocabulary: %s", specials["@vocab"])
        return False, reports, {}, {}
    elif specials.get("@vocab") is None:
        reports.append("Empty @context or @context/@vocab")
    # go through all fields / types and validate them
    reports += trawlObject(obj, aliases)
    # Valid object, plus any warnings
    return True, reports, aliases, specials


def validatePOSTObject(obj, headers):
    valid, reports, aliases, specials = validateGETObject(obj, headers)
    if ("@id" in obj) or ("id" in obj):
        reports.append("ID field present in POST")
    return valid, reports, aliases, specials
