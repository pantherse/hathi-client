
# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from activipy import vocab, core
from activipy.demos import dbm

db = dbm.JsonDBM.open("/tmp/test.db")
db["foo"] = {"cat": "meow", "dog": "woof"}
print(db["foo"])

post_this = vocab.Create(
    "http://tsyesika.co.uk/act/foo-id-here/",
    actor=vocab.Person(
        "http://tsyesika.co.uk/",
        displayName="Jessica Tallon"),
    to=["acct:cwebber@identi.ca",
        "acct:justaguy@rhiaro.co.uk",
        "acct:ladyaeva@hedgehog.example"],
    object=vocab.Note(
        "htp://tsyesika.co.uk/chat/sup-yo/",
        content="Up for some root beer floats?"))

print(post_this.json())

post_this = core.ASObj(post_this.json(), dbm.DbmNormalizedEnv)
post_this.m.save(db)
# is the same as dbm.DvmEnv.m.save(post_this, db)
print('now we can print the saved object')
print( db["http://tsyesika.co.uk/act/foo-id-here/"])

# now lets define a way to pull it back out
def dbm_fetch(id, db, env):
    return core.ASObj(db[id], env)
def dbm_fetch_denormalized(id, db, env):
    return env.m.denormalize(dbm_fetch(id, db, env), db)

denormalized_post = dbm_fetch_denormalized("http://tsyesika.co.uk/act/foo-id-here/",db, dbm.DbmNormalizedEnv)

# and we can print that
print(denormalized_post.json())
