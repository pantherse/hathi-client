#!/usr/bin/env python3

# Copyright 2018-present Hathi authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import dbLib as db
import sqlite3
import unittest

conn = sqlite3.connect('test.db')

class TestStringMethods(unittest.TestCase):
    test_db = db.database('test.db')
    conn = sqlite3.connect('test.db')
    def test_add_user(self):
        self.__class__.test_db.add_user("Keane Wolter", "kwolter")
        cursor = self.__class__.conn.cursor()
        cursor.execute("SELECT address FROM users")
        self.assertEqual(cursor.fetchone()[0], "kwolter")

    def test_get_list(self):
        db = self.__class__.test_db.get_list()
        cursor = self.__class__.conn.cursor()
        results = []
        for row in cursor.execute("SELECT name FROM users"):
            results.append(row)
        self.assertEqual(db, results)

    def test_delete_user_address(self):
        self.__class__.test_db.del_user_address("kwolter")
        cursor = self.__class__.conn.cursor()
        cursor.execute("SELECT address FROM users")
        self.assertEqual(cursor.fetchone(), None)

    def test_invalid_delete_user_address(self):
        self.assertRaises(ValueError, self.__class__.test_db.del_user_address("kwolter"))

if __name__ == '__main__':
    unittest.main()
